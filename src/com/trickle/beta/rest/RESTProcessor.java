package com.trickle.beta.rest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class RESTProcessor {
	
	RESTService service;
	RESTQueryDatabase db;
	
	public RESTProcessor(RESTService service){
		this.service = service;
		
		db = new RESTQueryDatabase(service.getApplicationContext());
	}
	
	public void processRequest(Intent intent){
		// Assuming network operation takes more time
		// than inserting into db
		
		// Putting empty response in the bundle
		Bundle bundle = intent.getExtras();
		bundle.putString(RESTConstants.RESPONSE_DATA, "");
		bundle.putInt(RESTConstants.REQUEST_STATUS, RESTConstants.REQUEST_STATUS_PENDING);
		
		pushDataToContentProvider(bundle);
		doRESTCall(intent.getExtras());
	}
	
	private void processRESTResponse(Bundle data){
		pushDataToContentProvider(data);
	}
	
	private void pushDataToContentProvider(Bundle data){
		new InsertDataAsyncTask().execute(db,  data);
	}
	
	private void doRESTCall(Bundle data){
		new RESTCallAsyncTask().execute(data);
	}
	

	private class InsertDataAsyncTask extends AsyncTask<Object, Void, Bundle> {
        protected void onPreExecute() {
        }
        
        protected Bundle doInBackground(Object... params) {
        	RESTQueryDatabase db = (RESTQueryDatabase) params[0];
        	Bundle data = (Bundle) params[1];
        	
        	synchronized (db) {
        		long randomToken = new Random().nextLong();
        		db.open(randomToken);
            	db.addOrUpdateRestData(data.getInt(RESTConstants.REQUEST_ID), 
            			data.getString(RESTConstants.RESPONSE_DATA), 
            			data.getInt(RESTConstants.REQUEST_STATUS));
            	db.close(randomToken);
			}
            return data;
        }
        
        protected void onPostExecute(Bundle data) {
        	// Inform listeners only when request is completed
        	if(data.getInt(RESTConstants.REQUEST_STATUS) != RESTConstants.REQUEST_STATUS_PENDING)
        		service.requestCallback(0, data);
        }
    }
	
	private class RESTCallAsyncTask extends AsyncTask<Object, Void, Bundle> {
		
		Bundle data;
		
        protected void onPreExecute() {
        }
        
        protected Bundle doInBackground(Object... params){
        	data = (Bundle) params[0];
        	
        	String url = data.getString("URL");
        	
        	HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 5000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 8000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpUriRequest httprequest=null;
            httprequest= new HttpPost(url);   
            
            httprequest.setHeader("User-Agent", "Trickle");
            
            Log.i("RESTProcessor", "url: " + url);
            if(data.containsKey("PostData")){
        		try{
        			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); 
        			JSONObject postData = new JSONObject(data.getString("PostData"));
        			Iterator i = postData.keys();
        			while (i.hasNext()) {
						String name = (String)i.next();
						nameValuePairs.add(new BasicNameValuePair(name, postData.getString(name)));
						Log.i("POST-PARAMS", "Variable: " + name + ":" + postData.getString(name));
					}
        			((HttpPost)httprequest).setEntity(new UrlEncodedFormEntity(nameValuePairs));
        		}catch(JSONException exp){
        			// TODO: handle exception
        		} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
            
            else if(data.containsKey("PostBody")){
        		try{
        			httprequest.addHeader("Content-Type", 
        			        "application/json");
        			
        			Log.i("POST-BODY", data.getString("PostBody"));
        			if(data.containsKey("PostBodyGet")){
        				url = url + "?";
	        			JSONObject postBodyGet = new JSONObject(data.getString("PostBodyGet"));
	        			Iterator i = postBodyGet.keys();
	        			while (i.hasNext()) {
							String name = (String)i.next();
							url = url + name + "=" + postBodyGet.getString(name) + "&";
							Log.i("POST-BODY-GET", "Variable: " + name + ":" + postBodyGet.getString(name));
						}
	        			
	        			url = url.substring(0, url.length()-1);
	        			httprequest= new HttpPost(url);  
	                    httprequest.setHeader("User-Agent", "Trickle");
        			}
        			
        			StringEntity strEntity = new StringEntity(data.getString("PostBody"));
        			

        			((HttpPost)httprequest).setEntity(strEntity);
        		} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}

            else{
        		httprequest = new HttpGet(url);
        	}
            
            try {
                HttpResponse response = httpClient.execute(httprequest);
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                StringBuilder builder = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                String str = builder.toString();
                data.putString(RESTConstants.RESPONSE_DATA, str);
                data.putInt(RESTConstants.REQUEST_STATUS, RESTConstants.REQUEST_STATUS_COMPLETED);
                data.putInt(RESTConstants.RESPONSE_CODE, response.getStatusLine().getStatusCode());
                Log.i("RESTProcessor ", "Url: "+url+"Server Response: " + str);
                return data;
            } catch (Exception ex) {
            	ex.printStackTrace();
            	data.putInt(RESTConstants.REQUEST_STATUS, RESTConstants.REQUEST_STATUS_FAILED);
                return data;
            }
        }
        
        protected void onPostExecute(Bundle serverResponseBundle) {
        	processRESTResponse(serverResponseBundle);
        }
    }
	
}
