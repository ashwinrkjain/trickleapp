package com.trickle.beta.rest;

import java.util.HashSet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RESTDatabaseHandle extends SQLiteOpenHelper{
	private static final String DB_NAME = "rest_transitainment.db";
	private static final int DB_VERSION = 1;

	private static final String RESTDATA_CREATESQL = "CREATE TABLE RestData (_id TEXT PRIMARY KEY, response TEXT, status INTEGER)";
	public RESTDatabaseHandle(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(RESTDATA_CREATESQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		DebugLogger.w("Example", "Upgrading database, this will drop tables and recreate.");
	}

	private static RESTDatabaseHandle mDBHandle;
	private static HashSet<Long> mInterestedContexts = new HashSet<Long>();

	public synchronized static RESTDatabaseHandle getInstance(Context context) {
		if (mDBHandle == null) mDBHandle = new RESTDatabaseHandle(context.getApplicationContext());
		return mDBHandle;
	}

	public synchronized static SQLiteDatabase openDB(Context context, long randomToken) {
		mInterestedContexts.add(randomToken); // Tracks contexts
		return getInstance(context).getWritableDatabase();
	}
	
	public synchronized static SQLiteDatabase openReadableDB(Context context, long randomToken) {
		mInterestedContexts.add(randomToken); // Tracks contexts
		return getInstance(context).getReadableDatabase();
	}
	
	public synchronized static boolean closeReadableDB(Context context, long randomToken) {
		mInterestedContexts.remove(randomToken);
		if (mInterestedContexts.size() == 0) {
			getInstance(context).getReadableDatabase().close();
			return true;
		}
		return false;
	}

	public synchronized static boolean closeDB(Context context, long randomToken) {
		mInterestedContexts.remove(randomToken);
		if (mInterestedContexts.size() == 0) {
			getInstance(context).getWritableDatabase().close();
			return true;
		}
		return false;
	}
}
