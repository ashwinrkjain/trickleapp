package com.trickle.beta.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.params.CoreProtocolPNames;

import com.trickle.beta.model.Category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.webkit.WebSettings;


public class RESTServiceHelper {

	static RESTServiceHelper singletonObject;

	public static final String baseURL = "http://54.214.143.226:8080/TrickleServer";
	//public static final String baseURL = "http://192.168.43.254:5050";
	public static final String loginURL = "/api/auth";
	public static final String syncURL = "/api/sync";
	private static final String signupURL = "/api/signup";
	
	ArrayList<Integer> pendingRequestsIds;

	HashMap<Integer, ArrayList<RESTRequestListeners>> listeners;

	

	private RESTServiceHelper(){
		pendingRequestsIds = new ArrayList<Integer>();
		listeners= new HashMap<Integer, ArrayList<RESTRequestListeners>>();
	}

	public static RESTServiceHelper getRESTServiceHelper(){
		if(singletonObject==null){
			singletonObject = new RESTServiceHelper();
		}
		return singletonObject;
	}

	public int login(Context context, String username, String password, RESTRequestListeners restRequestListener){
		String url = getBaseURL() + loginURL;// + "?auth="+getAuthParams(username, password);
		
		Bundle extras = new Bundle();
		extras.putString("PostBody", "{\"username\": \""+username+"\"," +
				"\"password\": \""+password+"\"}");
		
		return addListenerAddExtrasAndStartService(url, extras, context, restRequestListener);
	}
	
	public int signUp(Context context, String emailid ,String username, String password, RESTRequestListeners restRequestListener){
		String url = getBaseURL() + signupURL;// + "?auth="+getAuthParams(username, password);
		
		Bundle extras = new Bundle();
		extras.putString("PostBody", "{\"username\": \""+username+"\"," +
				"\"password\": \""+password+"\"" + ","+
				"\"email\": \""+emailid+"\"}");
		
		return addListenerAddExtrasAndStartService(url, extras, context, restRequestListener);
	}
	
	/**
	 * Sends data passed to it to the sync URL in the form of a JSON object. 
	 * 
	 * @param context
	 * @param data - Formatted string to be directly passed as a JSON object
	 * @param token 
	 * @param restRequestListener
	 * @return
	 */
	public int syncAll(Context context, String token,  String data, RESTRequestListeners restRequestListener){
		String url = getBaseURL() + syncURL;
		
		Bundle extras = new Bundle();
		extras.putString("PostBody", data);
		extras.putString("PostBodyGet", "{\"token\":\""+token+"\"}");
		
		return addListenerAddExtrasAndStartService(url, extras, context, restRequestListener);
	}

	public int addListenerAddExtrasAndStartService(String url/*resourceId*/, Bundle extras, 
			Context context, RESTRequestListeners restRequestListener){
		String resourceId = url;
		int requestId = new Random().nextInt();
		ArrayList<RESTRequestListeners> requestListners = listeners.get(requestId);

		if(!pendingRequestsIds.contains(requestId)){
			Intent intent = prepareIntent(context, resourceId, requestId);
			if(extras!=null)
				intent.putExtras(extras);
			context.startService(intent);

			pendingRequestsIds.add(requestId);
			requestListners = new ArrayList<RESTRequestListeners>();
			listeners.put(requestId, requestListners);
		}
		requestListners.add(restRequestListener);

		return requestId;
	}

	public String getBaseURL(){
		return baseURL;
	}

	public Intent prepareIntent(Context context, 
			/*Resource we want to query*/
			String resourceId, 
			/*Request id of the request*/
			int requestId){
		Intent intent = new Intent();
		intent.setClass(context, RESTService.class);
		intent.putExtra(RESTConstants.REQUEST_ID, requestId);
		intent.putExtra("URL", resourceId);

		ResultCallback callback = new ResultCallback(null);

		intent.putExtra(RESTConstants.BUNDLE_CALLBACK, callback);
		return intent;
	}

	private class ResultCallback extends ResultReceiver{
		public ResultCallback(Handler handler) {
			super(handler);
		}

		@Override
		public void onReceiveResult(int resultCode, Bundle resultData){

			int requestId = resultData.getInt(RESTConstants.REQUEST_ID);
			int responseCode = resultData.getInt(RESTConstants.RESPONSE_CODE);

			ArrayList<RESTRequestListeners> requestListeners = listeners.get(requestId);
			for(RESTRequestListeners listener: requestListeners){
				listener.requestCompleted(requestId, responseCode);
			}

			listeners.remove(new Integer(requestId));
			pendingRequestsIds.remove(new Integer(requestId));
		}
	}
}
