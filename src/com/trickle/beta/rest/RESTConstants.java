package com.trickle.beta.rest;

public class RESTConstants {
	
	public static final String REQUEST_STATUS = "request_status";
	
	public static final int REQUEST_STATUS_COMPLETED = 1;
	public static final int REQUEST_STATUS_PENDING = 2;
	public static final int REQUEST_STATUS_FAILED= 3;
	
	public static final String REQUEST_ID = "request_id";
	public static final String RESPONSE_CODE = "response_code";
	public static final String BUNDLE_CALLBACK = "Callback";
	public static final String RESPONSE_DATA = "response_data";
	
}
