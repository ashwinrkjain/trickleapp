package com.trickle.beta.rest;

public interface RESTRequestListeners {
	public int requestCompleted(int requestCode, int responseCode);
}
