package com.trickle.beta.rest;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.util.Log;

public class RESTService extends Service{
	 
	private final String LOG_TAG = "RESTService";

	private final IBinder mBinder = new RESTServiceBinder();

	private RESTProcessor processor;

	@Override
	public void onCreate(){
		super.onCreate();
		Log.i(LOG_TAG, "onCreate()");
		processor = new RESTProcessor(this);
	}

	public int onStartCommand(Intent intent, int flags, int startId){
		if(intent==null){
			Log.i(LOG_TAG, "null intent");
		}else{
			Log.i(LOG_TAG, "processing intent");
			processor.processRequest(intent);
		}
		
		
		return START_NOT_STICKY;
	}
	
	public void requestCallback(int resultCode, Bundle b){
		ResultReceiver rr = (ResultReceiver)b.getParcelable(RESTConstants.BUNDLE_CALLBACK);
		rr.send(resultCode, b);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Log.i("RESTService", "onDestroy()");
	}

	public class RESTServiceBinder extends Binder {
		RESTService getService() {
			return RESTService.this;
		}
	}

}
