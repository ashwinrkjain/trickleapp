package com.trickle.beta.rest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RESTQueryDatabase {
private static String TAG = "RESTQueryDatabase";
    
    Context mContext;
    
    public RESTQueryDatabase(Context context) {
        mContext = context;
    }
    
    synchronized public void open(long randomToken) {
        RESTDatabaseHandle.openDB(mContext, randomToken);
    }
        
    synchronized public SQLiteDatabase getReadableDB() {
        SQLiteDatabase db = RESTDatabaseHandle.getInstance(mContext).getReadableDatabase();
        assert (db.isOpen());
        return db;
    }
    
    synchronized public SQLiteDatabase getWriteableDB() {
        SQLiteDatabase db = RESTDatabaseHandle.getInstance(mContext).getWritableDatabase();
        assert (db.isOpen());
        return db;
    }
    
    synchronized public void close(long randomToken) {
        RESTDatabaseHandle.closeDB(mContext, randomToken);
    }
    
    public void clearDB() {
        SQLiteDatabase mDB = getWriteableDB();
        mDB.beginTransaction();
        try {
            mDB.delete("RestData", null, null);
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mDB.endTransaction();
        }
    }
    
    synchronized public int getQueryStatus(int requestId) {
        Cursor c= getReadableDB().rawQuery("SELECT status from RestData where _id=?", new String[]{requestId+""}); 
        c.moveToFirst();
        int status =c.getInt(c.getColumnIndexOrThrow("status"));
        c.close();
        return status; 
    }
     
    synchronized public String getServerResponse(int requestId) throws RESTException{
    	SQLiteDatabase mDB = getReadableDB();
    	Log.i("RESTQueryDatabase", "database state: " + mDB.isOpen());
    	Cursor c= mDB.rawQuery("SELECT * from RestData where _id=?", new String[] {requestId+""});
    	c.moveToFirst();
        int status = c.getInt(c.getColumnIndexOrThrow("status"));
        if(status == RESTConstants.REQUEST_STATUS_COMPLETED){
        	String response = c.getString(c.getColumnIndexOrThrow("response"));
        	c.close();
        	return response;
        }else if(status == RESTConstants.REQUEST_STATUS_FAILED){
        	c.close();
        	return "";
        }else{
        	c.close();
        	throw new RESTException("Request status pending");
        }
    }
    
    synchronized public void removeRequest(int requestId) {
        SQLiteDatabase mDB = getWriteableDB();
        mDB.beginTransaction();
        try {
            mDB.delete("RestData", "_id=?", new String[] {requestId+""});
            mDB.setTransactionSuccessful();
        } finally {
            mDB.endTransaction();
        }
    }
    
    synchronized private void addOrUpdateTable(String tablename, ContentValues values, String id) {
        SQLiteDatabase mDB = getReadableDB();
         
        Cursor cursor = mDB.query(tablename, new String[] { "_id" }, "_id=?", new String[] { id }, null, null, null);//mDB.rawQuery("select * from "+tablename+" where _id = ?", new String[]{id});//
        int n = cursor.getCount();
        cursor.close();
        
        mDB = getWriteableDB(); 
        mDB.beginTransaction();
        try {
            if (n == 0) {
                values.put("_id", id);
                mDB.insert(tablename, null, values);
            } else {
                values.remove("_id");
                mDB.update(tablename, values, "_id=?", new String[] { id });
            }
            mDB.setTransactionSuccessful();
        } finally {
            mDB.endTransaction();
        }
    }
    
    public void addOrUpdateRestData(int requestId, String response, int status) {
    	Log.i(TAG, "Adding/Updating request having requestId:" + requestId);
        
        ContentValues vals = new ContentValues();
        vals.put("_id", requestId+"");
        vals.put("response", response);
        vals.put("status", status);
        
        addOrUpdateTable("RestData", vals, requestId+"");
    }
}
