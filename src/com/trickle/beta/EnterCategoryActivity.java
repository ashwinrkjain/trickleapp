package com.trickle.beta;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.trickle.beta.model.Category;

public class EnterCategoryActivity extends Activity{

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_category);
	}

	public void onCreateCategory(View view) {
		Double categoryBudget = Double.parseDouble(((EditText)findViewById(R.id.category_budget)).getText().toString());
		String categoryName = ((EditText)findViewById(R.id.category_name)).getText().toString();
		String categoryDescription = ((EditText)findViewById(R.id.category_description)).getText().toString();

		Category category = new Category(null, categoryName, categoryBudget, categoryDescription, 0 , new Date(), new Date());
		category.save(getApplication());

		//Reset values
		((EditText)findViewById(R.id.category_budget)).setText("");
		((EditText)findViewById(R.id.category_name)).setText("");
		((EditText)findViewById(R.id.category_description)).setText("");

		TrickleApp.showToast(this, "Category saved");

		sendResult(RESULT_OK, false);
	}
	
	public void onCancel(View view){
		sendResult(RESULT_CANCELED, false);
	}
	
	@Override
	public void onBackPressed(){
		sendResult(RESULT_CANCELED, false);
	}
	
	public void sendResult(int resultStatus, boolean result){
		Intent returnIntent = new Intent();
		returnIntent.putExtra("result",result);
		setResult(resultStatus, returnIntent);        
		finish();		
	}
}
