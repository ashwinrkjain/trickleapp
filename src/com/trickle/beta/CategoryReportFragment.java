package com.trickle.beta;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Spinner;

import com.trickle.beta.adapters.DropDownMonthAdapter;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;

public class CategoryReportFragment extends Fragment {
	
	ArrayList<Category> categories = null;
	View finalView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		getActivity().setTitle("All Categories");
		View finalView = inflater.inflate(R.layout.category_reports, container, false);
		this.finalView = finalView;
				
		updateViewItems(finalView);
		Calendar calendar = Calendar.getInstance();
		Spinner spinner =  (Spinner) finalView.findViewById(R.id.report_month);
		DropDownMonthAdapter dropDownMonthAdapter = DropDownMonthAdapter.getDropDownMonthAdapter(calendar, getActivity());
		spinner.setAdapter(dropDownMonthAdapter);
		spinner.setOnItemSelectedListener(onItemSelectedListener);
		return finalView; 
	}
	
	OnItemClickListener onClickListener = new OnItemClickListener() {
		
		public void onItemClick(AdapterView<?> parent, View view, int position, long id){
			redirectToExpensesList(view , position);
		}
		
	};
	
	OnItemSelectedListener onItemSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			updateViewItems(getView());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private void redirectToExpensesList(View view, int position) {
		Intent intent = new Intent(view.getContext(), ExpenseCardsActivity.class);
		intent.putExtra(ExpensesListActivity.EXPENSE_POSITION, categories.get(position).getId());
		view.getContext().startActivity(intent);
	}
	
	Comparator<Category> alphabeticalComparator = new Comparator<Category>() {

		@Override
		public int compare(Category lhs, Category rhs) {
			return lhs.getCategoryName().compareTo(rhs.getCategoryName());
		}
		
	}; 
	
	private void updateViewItems(View finalView){
		if(finalView!= null){
			ListView listView = (ListView) finalView.findViewById(R.id.MyListView);
			
			Spinner spinner =  (Spinner) finalView.findViewById(R.id.report_month);
			BucketDataSource bucketDataSource = ((TrickleApp)getActivity().getApplication()).getDataSource();
			
			if(spinner.getSelectedItem() != null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(Long.parseLong((String)spinner.getSelectedItem()));
				bucketDataSource.setCurrentMonth(calendar);
				bucketDataSource.refresh(getActivity());
			}
			
			categories = new ArrayList<Category>(bucketDataSource.getCategories(true));
			Collections.sort(categories, alphabeticalComparator);
			listView.setAdapter(new CategoryReportListAdapter(categories, this.getActivity()));
			
			listView.setOnItemClickListener(onClickListener);
		}
	}
}
