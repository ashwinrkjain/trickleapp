package com.trickle.beta;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;
import com.trickle.beta.ui.TextProgressBar;

public class ExpenseDetailAdapter extends ArrayAdapter<Expense> {
	
	private Context context;
	private Expense[] expenses;
	private int rowLayout;
	  
	public ExpenseDetailAdapter(Context context, Expense[] expenses) {
		super(context, R.layout.row2, expenses);
		this.context = context;
	    this.expenses = expenses;
	    rowLayout = R.layout.row2;
	}

	 @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		 
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(rowLayout, parent, false);
	    
	    TextView expenseDescription = (TextView) rowView.findViewById(R.id.expense_description);
	    TextView expenseLocation = (TextView) rowView.findViewById(R.id.expense_location);
	    TextView expenseAmount = (TextView) rowView.findViewById(R.id.expense_amount);
	    
	    
	    expenseDescription.setText(expenses[position].getExpenseDescription());
	    expenseAmount.setText(String.valueOf(expenses[position].getExpenseAmount()));
	    expenseLocation.setText("("+expenses[position].getExpenseLocation()+")");
	    
	    return rowView;
	  }

	public void setExpensesList(Expense[] expenses) {
		this.expenses = expenses;
	}
	
	@Override
	public int getCount() {
		return expenses.length;
	}
	
	public void makeRowsDeletable(){
		rowLayout = R.layout.row_deletable;
	}

}
