package com.trickle.beta;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;
import com.trickle.beta.model.SyncableObject;
import com.trickle.beta.model.SyncableObject.Condition;
import com.trickle.beta.model.SyncableObjectFactory;

public class SyncServiceHelper {
	
	public static JSONObject syncAll(Class className, BucketDataSource dataSource, Context context, JSONObject allObjects) {
		if(allObjects == null){
			allObjects = new JSONObject();
		}
		
		ArrayList<SyncableObject> syncableObjects = null;
		Iterator<SyncableObject> iteratorOverSyncableObjects = null;
		SyncableObject syncableObject = null;
		
		//All new objects first
		JSONArray tempObjects = new JSONArray();
		try {
			syncableObjects = SyncableObjectFactory.getSyncableObjects(dataSource, context, className);
			iteratorOverSyncableObjects = syncableObjects.iterator();
			while(iteratorOverSyncableObjects.hasNext()){
				syncableObject = iteratorOverSyncableObjects.next();
				tempObjects.put(syncableObject.convertToJSON());
				syncableObject.setSynced(5); //Temporary status - sent for sync
			}
			
			if(className.equals(Expense.class)){
				allObjects.put("expenses", tempObjects);
			} else if(className.equals(Category.class)){
				allObjects.put("categories", tempObjects);
			}
			
			
		} catch (NoSuchFieldException e) {
			Log.e(className.getName(), "Table name missing in class" + className.getSimpleName());
		} catch (IllegalArgumentException e) {
			Log.e(className.getName(), "Error inserting values in JSON object");
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e(className.getName(), "Error inserting values in JSON object");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e(className.getName(), "Error inserting values in JSON object");
			e.printStackTrace();
		}
		
		return allObjects;
		
	}

	public static void handleResponse(JSONObject jsonResponse, Application application) throws JSONException {
		if(jsonResponse != null){
			
			markSyncingAsComplete(application, Category.class);
			markSyncingAsComplete(application, Expense.class);
			
			JSONArray arrayOfCategories = (JSONArray)jsonResponse.get("categories");
			JSONObject jsonCategory = null;
			Category category = null;
			
			for(int i=0; i<arrayOfCategories.length();i++){
				jsonCategory = arrayOfCategories.getJSONObject(i);
				category = new Category(jsonCategory);
				category.save(application);
			}
			
			JSONArray arrayOfExpenses = (JSONArray)jsonResponse.get("expenses");
			JSONObject jsonExpense = null;
			Expense expense = null;
			
			for(int i=0; i<arrayOfExpenses.length();i++){
				jsonExpense = arrayOfExpenses.getJSONObject(i);
				expense = new Expense(jsonExpense);
				expense.save(application);
			}
		}else {
			Log.e("SyncServiceHelper", "jsonResponse was null!");
		}
		
	}

	private static void markSyncingAsComplete(Application application, Class className) {
		
		BucketDataSource dataSource = BucketDataSource.getBucketDataSource((TrickleApp)application);
		
		try {
			ArrayList<SyncableObject> syncingObjects = SyncableObjectFactory.getSyncingObjects(dataSource, application.getApplicationContext(), className);
			SyncableObject syncableObject = null;
			for(int i=0; i<syncingObjects.size(); i++){
				syncableObject = syncingObjects.get(i);
				syncableObject.setSynced(10);
				syncableObject.save(application);
			}
			
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
	}

}
