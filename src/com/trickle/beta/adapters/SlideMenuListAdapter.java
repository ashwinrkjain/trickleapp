package com.trickle.beta.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trickle.beta.R;
import com.trickle.beta.SlideMenuItem;

/**
 * Created by ashwinjain on 30/05/13.
 */
public class SlideMenuListAdapter extends ArrayAdapter<SlideMenuItem> {

    public SlideMenuListAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.sm_list_row, null);
        }

        ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
        TextView title = (TextView) convertView.findViewById(R.id.row_title);

        title.setText(getItem(position).tag);
        if(!getItem(position).isTitle){
            icon.setImageResource(getItem(position).iconRes);
        }else{
            icon.setVisibility(View.GONE);
            title.setTypeface(Typeface.DEFAULT_BOLD);
        }
        return convertView;
    }

    public boolean isEnabled(int position){
        return !getItem(position).isTitle;
    }
}