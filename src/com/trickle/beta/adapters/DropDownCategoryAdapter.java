package com.trickle.beta.adapters;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;

import com.trickle.beta.model.Category;

public class DropDownCategoryAdapter extends DropDownGenericAdapter{
	
	ArrayList<Category> categories;
	ArrayList<String> textArray;
	ArrayList<String> valuesArray;
	
	private DropDownCategoryAdapter(ArrayList<String> text, ArrayList<String> values,
			ArrayList<Category> categories, Activity context){
		super(text,values, context);
		this.categories = categories;
		this.textArray  =text;
		this.valuesArray = values;
	}	
	
	//Constructor delegator pattern
	//(Thanks to Hari)
	public static DropDownCategoryAdapter getDropDownCategoryAdapter(ArrayList<Category> categories,
			Activity context){
		ArrayList<String> textArray = new ArrayList<String>();
		ArrayList<String> valuesArray = new ArrayList<String>();
		
		Iterator<Category> iteratorOverCategories = categories.iterator();
		Category category = null;
		while (iteratorOverCategories.hasNext()){
			category = iteratorOverCategories.next();
			textArray.add(category.getCategoryName());
			valuesArray.add(category.getId());
		}
		return new DropDownCategoryAdapter(textArray, valuesArray, categories, context);
	}
	
	@Override
	public void notifyDataSetChanged(){
		
		textArray.clear();
		valuesArray.clear();
		Iterator<Category> iteratorOverCategories = categories.iterator();
		Category category = null;
		while (iteratorOverCategories.hasNext()){
			category = iteratorOverCategories.next();
			textArray.add(category.getCategoryName());
			valuesArray.add(category.getId());
		}
		super.notifyDataSetChanged();
	}

}
