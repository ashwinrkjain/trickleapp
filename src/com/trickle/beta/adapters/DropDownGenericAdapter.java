package com.trickle.beta.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trickle.beta.R;

public class DropDownGenericAdapter extends BaseAdapter{

    ArrayList<String> text;
    ArrayList<String> values;
    Activity context;
    
    public DropDownGenericAdapter(ArrayList<String> text, ArrayList<String> values, Activity context){
        this.text = text;
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Object getItem(int index) {
        return values.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	LayoutInflater inflater = context.getLayoutInflater();
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.dropdown_listitem, null);
		}

		TextView categoryTitle = (TextView)view.findViewById(R.id.categoryTitle);
		categoryTitle.setText(text.get(position));		
		return view;		
    }
}