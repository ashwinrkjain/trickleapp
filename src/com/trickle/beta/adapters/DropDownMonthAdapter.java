package com.trickle.beta.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;

import com.trickle.beta.model.Category;

public class DropDownMonthAdapter extends DropDownGenericAdapter{
	
	ArrayList<String> textArray;
	ArrayList<String> valuesArray;
	
	private DropDownMonthAdapter(ArrayList<String> text, ArrayList<String> values, Activity context){
		super(text,values, context);
		this.textArray  = text;
		this.valuesArray = values;
	}	
	
	//Constructor delegator pattern
	//(Thanks to Hari)
	public static DropDownMonthAdapter getDropDownMonthAdapter(Calendar calendar,
			Activity context){
		ArrayList<String> textArray = new ArrayList<String>();
		ArrayList<String> valuesArray = new ArrayList<String>();

		Date date = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
		
		for(int i=0;i<3;i++){
			date = new Date(calendar.getTimeInMillis());
			textArray.add(simpleDateFormat.format(date));
			valuesArray.add(String.valueOf(calendar.getTimeInMillis()));
			calendar.add(Calendar.MONTH, -1);
			
		}
	return new DropDownMonthAdapter(textArray, valuesArray, context);
	}
}
