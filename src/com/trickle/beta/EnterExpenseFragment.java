package com.trickle.beta;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.trickle.beta.adapters.DropDownCategoryAdapter;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EnterExpenseFragment extends Fragment{
	
	private static final int NOT_SYNCED = 0;
	private static final int DATE_DIALOG_ID = 999;
	ArrayList<Category> categories;
	BucketDataSource bucketDataSource;
	DropDownCategoryAdapter spinnerAdapter;
	Expense expense;
	Intent intent;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");


    /*private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
    protected LocationManager locationManager;*/


    @Override
	public void onAttach(Activity activity) {
		intent = activity.getIntent();
		if(intent!=null){
			expense = (Expense) intent.getSerializableExtra("EXPENSE");
		}
		super.onAttach(activity);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        getActivity().setTitle("Add Expense");

		View finalView = inflater.inflate(R.layout.enter_expenses, container, false);
		int position=0;
		
		Spinner spinner = (Spinner) finalView.findViewById(R.id.expense_category);
		bucketDataSource = BucketDataSource.getBucketDataSource((TrickleApp)getActivity().getApplication()); 
		categories = new ArrayList<Category>(bucketDataSource.getCategories(false));
		
		spinnerAdapter = DropDownCategoryAdapter.getDropDownCategoryAdapter(categories, getActivity());
		
		if (expense != null){
			((EditText)finalView.findViewById(R.id.expense_amount)).setText(String.valueOf(expense.getExpenseAmount()));
			((EditText)finalView.findViewById(R.id.expense_description)).setText(expense.getExpenseDescription());
			((AutoCompleteTextView)finalView.findViewById(R.id.expense_location)).setText(expense.getExpenseLocation());
			((TextView)finalView.findViewById(R.id.expense_date)).setText(simpleDateFormat.format(expense.getExpenseDate()));
			
			position = findIndex(categories, expense.getCategoryId());
		} else {
			((TextView)finalView.findViewById(R.id.expense_date)).setText(simpleDateFormat.format(new Date()));
		}
		spinner.setAdapter(spinnerAdapter);
		spinner.setSelection(position);

        setAutoCompleteForExpenseLocation((AutoCompleteTextView) finalView.findViewById(R.id.expense_location));
		
		return finalView; 
	}

    private void setAutoCompleteForExpenseLocation(AutoCompleteTextView textView){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_dropdown_item_1line, readStoredLocations());
        textView.setAdapter(adapter);
    }

    private String[] readStoredLocations(){
        return ((TrickleApp)getActivity().getApplication()).getUsedLocationList();
    }

    private void updateUsedLocationList(String newLocation){
        ((TrickleApp)getActivity().getApplication()).addUsedLocations(newLocation);
    }

	private int findIndex(ArrayList<Category> categories2, String categoryId) {
		for(int i=0; i<categories2.size(); i++){
			if(categories2.get(i).getId().equals(categoryId)){
				return i;
			}
		}
		return -1;
	}

	public void onCreateExpense() {
		Activity activity = getActivity();
		if(validate(activity)){
			String expenseId = null;
			Date expenseCreationDate = new Date();
			Date expenseDate = new Date();
			
			Double expenseAmount = Double.parseDouble(((EditText)activity.findViewById(R.id.expense_amount)).getText().toString());
			String expenseDescription =  ((EditText)activity.findViewById(R.id.expense_description)).getText().toString();
			String expenseLocation  = ((AutoCompleteTextView)activity.findViewById(R.id.expense_location)).getText().toString();
			String expenseCategory = (String)((Spinner)activity.findViewById(R.id.expense_category)).getSelectedItem();
			
			try {
				expenseDate = simpleDateFormat.parse(((TextView)activity.findViewById(R.id.expense_date)).getText().toString());
			} catch (ParseException e) {
				//Do nothing. Date is already initialized to today's date.
			}
			if(expense!=null){
				expenseId = expense.getId();
				expenseCreationDate = expense.getCreationDate();
			}
			expense = new Expense(expenseId, NOT_SYNCED, expenseAmount, expenseDescription, expenseLocation, 0.0, 9.9, expenseCategory, 
					expenseDate, expenseCreationDate, new Date());
			expense.save(activity.getApplication());

            updateUsedLocationList(expenseLocation);
	
			//Reset values
			((EditText)activity.findViewById(R.id.expense_amount)).setText("");
			((EditText)activity.findViewById(R.id.expense_description)).setText("");
			((AutoCompleteTextView)activity.findViewById(R.id.expense_location)).setText("");
			TrickleApp.showToast(activity, "Expense saved");
		}
	}
	
	public void onClearExpense() {
		Activity activity = getActivity();
		((EditText)activity.findViewById(R.id.expense_amount)).setText("");
		((EditText)activity.findViewById(R.id.expense_description)).setText("");
		((AutoCompleteTextView)activity.findViewById(R.id.expense_location)).setText("");
		
		((TextView)activity.findViewById(R.id.expense_date)).setText(simpleDateFormat.format(new Date()));
		
		expense = null;
	}
	
	private boolean validate(Activity activity) {
		if(((EditText)activity.findViewById(R.id.expense_amount)).getText().toString().trim().length() == 0){
			((EditText)activity.findViewById(R.id.expense_amount)).setError(TrickleApp.getSpannableStringBuilder("Amount cannot be blank"));
			
			return false;
		} 
		if(((EditText)activity.findViewById(R.id.expense_description)).getText().toString().trim().length() == 0){
			((EditText)activity.findViewById(R.id.expense_description)).setError(TrickleApp.getSpannableStringBuilder("Description cannot be blank"));
			return false;
		} 
		if(((AutoCompleteTextView)activity.findViewById(R.id.expense_location)).getText().toString().trim().length() == 0){
			((AutoCompleteTextView)activity.findViewById(R.id.expense_location)).setError(TrickleApp.getSpannableStringBuilder("Location cannot be blank"));
			return false;
		} 
		return true;
	}

	public void onAddCategory(View view){
		getActivity().startActivityForResult(new Intent(getActivity(), EnterCategoryActivity.class), Constants.ENTER_CATEGORY_FROM_EXPENSE);
	}


	public void handleAddCategoryResponse(int resultCode, Intent data){
		if(resultCode == android.app.Activity.RESULT_OK){
			categories.clear();
			categories.addAll(bucketDataSource.getCategories(false));
			spinnerAdapter.notifyDataSetChanged(); 
		}else if (resultCode == android.app.Activity.RESULT_CANCELED) {    
			//Write your code on no result return 
		}
	}
	

	OnDateSetListener onDateSetListener = new OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, selectedDay);
			calendar.set(Calendar.MONTH, selectedMonth);
			calendar.set(Calendar.YEAR, selectedYear);
			Activity activity = getActivity();
			((TextView)activity.findViewById(R.id.expense_date)).setText(simpleDateFormat.format(calendar.getTime()));
		}
	};
	
	public Dialog onCreateDialog(int id) {
		Calendar calendar = Calendar.getInstance();
		if(expense!=null){
			calendar.setTime(expense.getExpenseDate());
		}
		
		switch (id) {
		case DATE_DIALOG_ID:
		   // set date picker as current date
		   return new DatePickerDialog(getActivity(), onDateSetListener, 
                         calendar.get(Calendar.YEAR),
                         calendar.get(Calendar.MONTH),
                         calendar.get(Calendar.DATE));
		}
		
		return null;
	}

	public void onClickExpenseDate(View view) {
		getActivity().showDialog(DATE_DIALOG_ID);
	}

    /*private void registerLocationManager(){
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()
        );
    }

    protected void showCurrentLocation() {
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            Toast.makeText(this.getActivity(), message,
                    Toast.LENGTH_LONG).show();
        }
    }

    /*private class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {
            String message = String.format(
                    "New Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            location.get
            Toast.makeText(EnterExpenseFragment.this.getActivity(), message, Toast.LENGTH_LONG).show();
        }

        public void onStatusChanged(String s, int i, Bundle b) {
            Toast.makeText(EnterExpenseFragment.this.getActivity(), "Provider status changed",
                    Toast.LENGTH_LONG).show();
        }

        public void onProviderDisabled(String s) {
            Toast.makeText(EnterExpenseFragment.this.getActivity(),
                    "Provider disabled by the user. GPS turned off",
                    Toast.LENGTH_LONG).show();
        }

        public void onProviderEnabled(String s) {
            Toast.makeText(EnterExpenseFragment.this.getActivity(),
                    "Provider enabled by the user. GPS turned on",
                    Toast.LENGTH_LONG).show();
        }

    }*/

}
