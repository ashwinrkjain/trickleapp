package com.trickle.beta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SetupAccountActivity extends Activity {
	
	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        Intent service = new Intent(this, SyncService.class);
	        startService(service);
	        
	        setContentView(R.layout.setup_account);

	        
	    }
}
