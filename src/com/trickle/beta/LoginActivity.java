package com.trickle.beta;

import java.util.Random;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;
import com.trickle.beta.rest.RESTException;
import com.trickle.beta.rest.RESTQueryDatabase;
import com.trickle.beta.rest.RESTRequestListeners;
import com.trickle.beta.rest.RESTServiceHelper;


public class LoginActivity extends SherlockActivity{
    /** Called when the activity is first created */
	
	ProgressDialog loggingProgressDialog;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.main);
    }
	
	public void onLogin(View view){
		if(!validate())
    		return;
    	
		this.loggingProgressDialog = new ProgressDialog(this);
		this.loggingProgressDialog.setMessage(" Logging in ...");
		this.loggingProgressDialog.show();
		this.loggingProgressDialog.setCancelable(false);
		
    	String username = ((EditText)findViewById(R.id.username)).getText().toString();
    	String password = ((EditText)findViewById(R.id.password)).getText().toString();
    	RESTServiceHelper.getRESTServiceHelper().login(this, username, password, loginRESTListener);
    	
    	findViewById(R.id.username).setEnabled(false);
    	findViewById(R.id.password).setEnabled(false);
    	findViewById(R.id.login).setEnabled(false);
	}
	
	public boolean validate(){
		if(((EditText)findViewById(R.id.username)).getText().toString().trim().length() == 0){
			((EditText)findViewById(R.id.username)).setError(TrickleApp.getSpannableStringBuilder("Please enter username"));
			return false;
		}
		if(((EditText)findViewById(R.id.password)).getText().toString().length() == 0){
			((EditText)findViewById(R.id.password)).setError(TrickleApp.getSpannableStringBuilder("Please enter password."));
			return false;
		}
		return true;
	}
	
	RESTRequestListeners loginRESTListener = new RESTRequestListeners() {
		
		@Override
		public int requestCompleted(int requestId, int responseCode) {
			
			//loggingProgressDialog.dismiss();
			
			findViewById(R.id.username).setEnabled(true);
	    	findViewById(R.id.password).setEnabled(true);
	    	findViewById(R.id.login).setEnabled(true);
			if(responseCode == HttpStatus.SC_OK){
				//Query rest DB to get response
				RESTQueryDatabase db = new RESTQueryDatabase(getApplicationContext());
				try {
					long randomToken = new Random().nextLong();
					db.open(randomToken);
					String serverResponse = db.getServerResponse(requestId);
					db.close(randomToken);
					JSONObject jsonResponse = new JSONObject(serverResponse);
					if(jsonResponse.getBoolean("result")){
						TrickleApp.showToast(LoginActivity.this, "Login successful");
						loginUser(jsonResponse.getString("token"));
					}else{
						showErrorMessage(responseCode, "Error");
					}
				} catch (RESTException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}else{
				showErrorMessage(responseCode, "Error");
				return -1;
			}
			return 0;
		}
	};
	
	public void showErrorMessage(int responseCode, String message){
		loggingProgressDialog.dismiss();
		TrickleApp.showToast(this, responseCode+": " + message);
	}
	
	public void loginUser(String token){
		((TrickleApp)getApplication()).setToken(token);
		
		RESTServiceHelper restServiceHelper = RESTServiceHelper.getRESTServiceHelper();
		JSONObject json = new JSONObject();
		
		try {
			json.put("last_sync_time", ((TrickleApp)getApplication()).getLastSyncTime());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		restServiceHelper.syncAll(this,((TrickleApp)getApplication()).getToken(), 
				json.toString(), requestListener);
		this.loggingProgressDialog.setMessage("Syncing...");
		
		
	}
	
	RESTRequestListeners requestListener = new RESTRequestListeners() {
		
		@Override
		public int requestCompleted(int requestCode, int responseCode) {
			if(responseCode == HttpStatus.SC_OK){
				//Query rest DB to get response
				RESTQueryDatabase db = new RESTQueryDatabase(getApplicationContext());
				try {
					long randomToken = new Random().nextLong();
					db.open(randomToken);
					String serverResponse = db.getServerResponse(requestCode);
					db.close(randomToken);
					handleResponse(serverResponse);
					redirectToBaseTab();
					
				} catch (RESTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				return -1;
			}
			return 0;
		}

	};
	
	private void redirectToBaseTab() {
		loggingProgressDialog.dismiss();
		Intent intent = new Intent(LoginActivity.this, BaseActivity.class);
        startActivity(intent);
        
        finish();
	}
	public void handleResponse(String serverResponse) throws JSONException{
		JSONObject jsonResponse = new JSONObject(serverResponse);
		SyncServiceHelper.handleResponse(jsonResponse, getApplication());
		
		((TrickleApp)getApplication()).setLastSyncTime(jsonResponse.getLong("last_sync_time"));
	}
	
}