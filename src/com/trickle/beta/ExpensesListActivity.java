package com.trickle.beta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Expense;
import com.trickle.beta.ui.TouchListView;

public class ExpensesListActivity extends SherlockActivity {

	public static final String EXPENSE_POSITION = "Expense_Position";
	ExpenseDetailAdapter expenseDetailAdapter = null;
	Expense[] expenses = null;
	ArrayList<Expense> arrayListOfExpenses = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		setContentView(R.layout.expenses_list);
		
		TouchListView listView = (TouchListView) findViewById(R.id.MyListView);
		
		
		Intent intent = getIntent();
		String expensePosition = intent.getStringExtra(EXPENSE_POSITION);
		
		BucketDataSource bucketDataSource = BucketDataSource.getBucketDataSource((TrickleApp)getApplication());
		if(expensePosition == null || "-1".equals(expensePosition)){
			arrayListOfExpenses = new ArrayList<Expense>(bucketDataSource.getExpenses());
		} else {
			arrayListOfExpenses = new ArrayList<Expense>(bucketDataSource.getExpensesOfCategory(expensePosition));
		}
		Collections.sort(arrayListOfExpenses, dateComparator);
		expenses = arrayListOfExpenses.toArray(new Expense[arrayListOfExpenses.size()]);
		
		expenseDetailAdapter = new ExpenseDetailAdapter(this, expenses);
		listView.setAdapter(expenseDetailAdapter);
		listView.setDropListener(onDrop);
		listView.setRemoveListener(onRemove);
	//	listView.setOnLongClickListener(onLongClickListener);
		listView.setOnItemLongClickListener(onItemLongClickListener);
		listView.setOnItemClickListener(onItemClickListener);
	}
	
	
	OnItemClickListener onItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(view.getContext(), BaseActivity.class);
				intent.putExtra("EXPENSE", arrayListOfExpenses.get(position));
				view.getContext().startActivity(intent);
		}
	};

	private com.trickle.beta.ui.TouchListView.DropListener onDrop=new com.trickle.beta.ui.TouchListView.DropListener() {
		@Override
		public void drop(int from, int to) {
//				String item=expenseDetailAdapter.getItem(from);
//				
//				expenseDetailAdapter.remove(item);
//				expenseDetailAdapter.insert(item, to);
		}
	};
	
	private com.trickle.beta.ui.TouchListView.RemoveListener onRemove=new com.trickle.beta.ui.TouchListView.RemoveListener() {
		@Override
		public void remove(int which) {
			arrayListOfExpenses.remove(which);
			expenses = arrayListOfExpenses.toArray(new Expense[arrayListOfExpenses.size()]);
			expenseDetailAdapter.setExpensesList(expenses);
			expenseDetailAdapter.notifyDataSetChanged();	
//				expenseDetailAdapter.remove(expenseDetailAdapter.getItem(which));
		}
	};
	
//	OnLongClickListener onLongClickListener = new OnLongClickListener() {
//		
//		@Override
//		public boolean onLongClick(View v) {
//			expenseDetailAdapter.makeRowsDeletable();
//			expenseDetailAdapter.notifyDataSetChanged();
//			return false;
//		}
//	};
//	
	OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			expenseDetailAdapter.makeRowsDeletable();
			expenseDetailAdapter.notifyDataSetChanged();
			return true;
		}
	};
	
	Comparator<Expense> dateComparator = new Comparator<Expense>() {
		
		@Override
		public int compare(Expense lhs, Expense rhs) {
			if(lhs.getCreationDate().before(rhs.getCreationDate())){
				return -1;
			} else if (lhs.getCreationDate().after(rhs.getCreationDate())){
				return 1;
			} else{
				return 0;
			}
		}
	}; 
}
