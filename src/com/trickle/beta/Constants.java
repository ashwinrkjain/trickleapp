package com.trickle.beta;

public class Constants {

	public static int ENTER_CATEGORY_FROM_EXPENSE = 0; 

    public static String SHARED_PREF_PREF_NAME = "trickle_sp";
    public static String SHARED_PREF_LOCATION_LIST = "location_list";

    public static String MENU_EXPENSE_TITLE = "Expense";
    public static String MENU_ADD_EXPENSE = "Add Expense";
    public static String MENU_CATEGORY_TITLE = "Category reports";
    public static String MENU_CATEGORY_ALL = "All Categories";
    public static String MENU_SETTINGS_TITLE = "Settings";
}
