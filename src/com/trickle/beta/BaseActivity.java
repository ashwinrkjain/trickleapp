package com.trickle.beta;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.view.Window;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import static com.trickle.beta.Constants.MENU_ADD_EXPENSE;
import static com.trickle.beta.Constants.MENU_CATEGORY_ALL;
import static com.trickle.beta.Constants.MENU_SETTINGS_TITLE;

/**
 * Created by ashwinjain on 29/05/13.
 */
public class BaseActivity extends BaseMenuFragmentActivity{

    private Fragment mContent;

    public BaseActivity() {
        super(R.string.main_menu_title);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the Above View
        if (savedInstanceState != null)
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        if (mContent == null)
            mContent = Fragment.instantiate(this, EnterExpenseFragment.class.getName());

        // set the Above View
        setContentView(R.layout.base_content_frame);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mContent)
                .commit();

        final MainMenuListFragment mainMenuListFragment = (MainMenuListFragment)Fragment.instantiate(this, MainMenuListFragment.class.getName());
        // set the Behind View
        setBehindContentView(R.layout.sm_menu_frame);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, mainMenuListFragment)
                .commit();

        // customize the SlidingMenu
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mainMenuListFragment.setSlideMenuItemSelectedListener(new SlideMenuItemSelectedListener() {
            @Override
            public void onSlideMenuItemSelected(SlideMenuItem selectedItem) {
                if (selectedItem.tag.equals(MENU_SETTINGS_TITLE)) {
                    switchContent(Fragment.instantiate(BaseActivity.this, SettingsFragment.class.getName()));
                }else if(selectedItem.tag.equals(MENU_ADD_EXPENSE)){
                    switchContent(Fragment.instantiate(BaseActivity.this, EnterExpenseFragment.class.getName()));
                }else if(selectedItem.tag.equals(MENU_CATEGORY_ALL)){
                    switchContent(Fragment.instantiate(BaseActivity.this, CategoryReportFragment.class.getName()));
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    public void switchContent(Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        getSlidingMenu().showContent();
    }

    public void onCreateExpense(View view){
        ((EnterExpenseFragment)mContent).onCreateExpense();
    }

    public void onClearExpense(View view){
        ((EnterExpenseFragment)mContent).onClearExpense();
    }

    public void onAddCategory(View view){
        ((EnterExpenseFragment)mContent).onAddCategory(view);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ENTER_CATEGORY_FROM_EXPENSE) {
            ((EnterExpenseFragment)mContent).handleAddCategoryResponse(resultCode, data);
        }
    }

    public void onClickExpenseDate(View view){
        ((EnterExpenseFragment)mContent).onClickExpenseDate(view);
    }
    
    protected Dialog onCreateDialog(int id) {
		return ((EnterExpenseFragment)mContent).onCreateDialog(id);
	}

}
