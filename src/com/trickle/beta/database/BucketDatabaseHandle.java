package com.trickle.beta.database;

import java.util.HashSet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BucketDatabaseHandle extends SQLiteOpenHelper {
    private static final String DB_NAME = "trickle.db";
    private static final int DB_VERSION = 11;
    
    private static final String CATEGORY_CREATESQL = "CREATE TABLE Category (_id TEXT PRIMARY KEY, name TEXT, description TEXT, budget TEXT,  isSync INT DEFAULT 0, creationDate INTEGER, updateDate INTEGER)";
    private static final String EXPENSE_CREATESQL = "CREATE TABLE Expense   (_id TEXT PRIMARY KEY, categoryId TEXT, amount REAL, location TEXT, locationLat REAL, locationLong REAL, description TEXT, expenseDate INTEGER, creationDate INTEGER, isSync INT, updateDate INTEGER, FOREIGN KEY(categoryId) REFERENCES Categories(_id) ON UPDATE CASCADE)";
    
    public BucketDatabaseHandle(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CATEGORY_CREATESQL);
        db.execSQL(EXPENSE_CREATESQL);
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	Log.w("Example", "Upgrading database, this will drop tables and recreate.");
    	db.execSQL("DROP TABLE IF EXISTS Category");
    	db.execSQL("DROP TABLE IF EXISTS Expense");
        onCreate(db);
    }
    
    private static BucketDatabaseHandle mDBHandle;
    private static HashSet<Context> mInterestedContexts = new HashSet<Context>();
    
    public synchronized static BucketDatabaseHandle getInstance(Context context) {
        if (mDBHandle == null) mDBHandle = new BucketDatabaseHandle(context.getApplicationContext());
        return mDBHandle;
    }
    
    public synchronized static SQLiteDatabase openDB(Context context) {
        mInterestedContexts.add(context); // Tracks contexts
        return getInstance(context).getWritableDatabase();
    }
    
    public synchronized static boolean closeDB(Context context) {
        mInterestedContexts.remove(context);
        if (mInterestedContexts.size() == 0) {
            getInstance(context).getWritableDatabase().close();
            return true;
        }
        return false;
    }
}