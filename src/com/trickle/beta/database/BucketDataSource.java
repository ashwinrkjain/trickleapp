package com.trickle.beta.database;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.trickle.beta.TrickleApp;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;
import com.trickle.beta.model.SyncableObject;
import com.trickle.beta.model.SyncableObjectFactory;

public class BucketDataSource {
    private static String TAG = "BucketDataSource";
    private static String DefaultBucketSuffix = "Bucket";
    private static int MAX_BUCKET_SIZE_IN_HOURS = 3;
    
    private TrickleApp mApp;
    
    private Collection<Category> categories;
    private Collection<Expense> expenses;
    
    private TreeMap<String, Category> categoryMap;
    private TreeMap<String, Expense> expenseMap;
    
    private Calendar currentMonth;
    
    boolean mInitialized = false;
    
    private BucketDataSource(TrickleApp app) {
        mApp = app;
        
        categoryMap = new TreeMap<String, Category>();
        expenseMap = new TreeMap<String, Expense>();
        
        currentMonth = Calendar.getInstance();
        refresh(mApp.getApplicationContext());
        
        
    }
    
    public static BucketDataSource getBucketDataSource(TrickleApp app){
    	BucketDataSource bucketDataSource = app.getDataSource();
    	if(bucketDataSource == null){
    		bucketDataSource = new BucketDataSource(app);
    	}
    	return bucketDataSource;
    }
    
    public void refresh(Context context) {
            try {
            	BucketDatabase mDB = new BucketDatabase(context, mApp);
                Cursor cursor;
                
                categoryMap.clear();
                expenseMap.clear();
                
                cursor = mDB.queryAllCategories();
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    Category category = new Category(cursor);
                    categoryMap.put(category.getId(), category);
                    cursor.moveToNext();
                }
                
                cursor.close();
                cursor = mDB.queryAllExpenses(currentMonth);
                
                cursor.moveToFirst();
                
                for (int i = 0; i < cursor.getCount(); i++) {
                    Expense expense = (Expense) SyncableObjectFactory.getObjectFromCursor(cursor, Expense.class);
                    expenseMap.put(expense.getId(), expense);
                    categoryMap.get(expense.getCategoryId()).addExpense(expense);
                    cursor.moveToNext();
                }
                cursor.close();
                
                Category allExpenses = new Category("-1","All Expenses", -1, "All Expenses", 0, null, null);
                categoryMap.put(allExpenses.getId(), allExpenses);
                allExpenses.setExpensesInThisCategory(getExpenses());
                
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e("BucketDataSource", "Exception in datasource init: " + ex.getMessage());
            }
    }
    
    public Collection<Category> getCategories(boolean includeAllExpenses){
    	Collection<Category> categories = null;
    	if(includeAllExpenses){
    		categories = getCategories();
    	} else {
    		categories = getCategoriesExceptAllExpenses();
    	}
    	return categories;
    }
    
    private Collection<Category> getCategories(){
    	if(this.categories == null){
    		categories = categoryMap.values();
    	}
    	return this.categories;
    }
    
    private Collection<Category> getCategoriesExceptAllExpenses(){
    	Map exceptAllExpenses = (Map) categoryMap.clone();
    	if(exceptAllExpenses.containsKey("-1")){
    		exceptAllExpenses.remove("-1");
    	}
    	return exceptAllExpenses.values();
    }
    
    public Collection<Expense> getExpenses(){
    	if(this.expenses == null){
    		expenses =  expenseMap.values();
    	}
    	return this.expenses;
    }
    
    public Calendar getCurrentMonth() {
		return currentMonth;
	}

	public void setCurrentMonth(Calendar currentMonth) {
		this.currentMonth = currentMonth;
	}

	public void addSyncableObject(SyncableObject syncableObject, Context context){
    	BucketDatabase db = getDatabase(context);
    	db.saveSyncableObject(syncableObject);
    	
    	if(syncableObject instanceof Expense){
    		Expense expense = (Expense) syncableObject;
	    	expenseMap.put(expense.getId(), expense);
	    	categoryMap.get(expense.getCategoryId()).addExpense(expense);
    	} else if(syncableObject instanceof Category){
      		Category category = (Category)syncableObject;
        	categoryMap.put(category.getId(), category);
        	
    	}
    }

    public void removeSyncableObject(SyncableObject syncableObject, Context context){
        BucketDatabase db = getDatabase(context);
        db.removeSyncableObject(syncableObject);

        if(syncableObject instanceof Expense){
            Expense expense = (Expense) syncableObject;
            expenseMap.remove(expense.getId());
            categoryMap.get(expense.getCategoryId()).removeExpense(expense);
        }
    }
    
    public BucketDatabase getDatabase(Context context){
    	return new BucketDatabase(context, mApp);
    }
    
    public void closeDatabase(BucketDatabase database){
    	database.close();
    }
    
    public void purgeData(BucketDatabase mDB) {
        mDB.clearDB();
        Log.i("BucketDataSource", "Deleting Data");
        categories.clear();
    }

	public Collection<Expense> getExpensesOfCategory(String categoryId) {
		if(this.expenses == null){
    		expenses =  expenseMap.values();
    	}
		return categoryMap.get(categoryId).getExpenses();
	}
    
//	public Collection<Expense> getExpensesForMMYY(String mmyy) {
//		if(this.expenses == null){
//    		expenses =  expenseMap.values();
//    	}
//		return categoryMap.get(categoryId).getExpenses();
//	}
	
}