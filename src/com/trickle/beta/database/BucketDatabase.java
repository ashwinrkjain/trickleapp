package com.trickle.beta.database;

import java.util.Calendar;
import java.util.UUID;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trickle.beta.TrickleApp;
import com.trickle.beta.model.SyncableObject;

public class BucketDatabase {
	private static String TAG = "TrickleDB";

	Context mContext;
	TrickleApp mApp;

	public BucketDatabase(Context context, TrickleApp app) {
		mContext = context;
		mApp = app;
	}

	synchronized public void open() {
		BucketDatabaseHandle.openDB(mContext);
	}

	synchronized public SQLiteDatabase getReadableDB() {
		SQLiteDatabase db = BucketDatabaseHandle.getInstance(mContext).getReadableDatabase();
		assert (db.isOpen());
		return db;
	}

	synchronized public SQLiteDatabase getWriteableDB() {
		SQLiteDatabase db = BucketDatabaseHandle.getInstance(mContext).getWritableDatabase();
		assert (db.isOpen());
		return db;
	}

	synchronized public void close() {
		BucketDatabaseHandle.closeDB(mContext);
	}

	public void clearDB() {
		SQLiteDatabase mDB = getWriteableDB();
		mDB.beginTransaction();
		try {
			mDB.delete("Category", null, null);
			mDB.delete("Expense", null, null);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			mDB.endTransaction();
		}
	}

	public boolean isClosed(Context context) {
		return !BucketDatabaseHandle.getInstance(context).getReadableDatabase().isOpen();
	}
	
	public void saveSyncableObject(SyncableObject syncableObject){
		if(syncableObject.getId() == null || syncableObject.getId().equals("")){
    		UUID uuid = UUID.randomUUID();
    		syncableObject.setId(uuid.toString());
    		addNewSyncableObject(syncableObject);
    	} else {
    		addOrUpdateExistingSyncableObject(syncableObject);
    	}
	}

    public void removeSyncableObject(SyncableObject syncableObject){
        SQLiteDatabase mDB = getWriteableDB();
        mDB.beginTransaction();
        try {
            mDB.delete(syncableObject.getTableName(), "_id=?", new String[] { syncableObject.getId() });
            mDB.setTransactionSuccessful();
        } finally {
            mDB.endTransaction();
        }
    }
	
	private void addOrUpdateTable(String tablename, ContentValues values, String id) {
        SQLiteDatabase mDB = getReadableDB();
        Cursor cursor = mDB.query(tablename, new String[] { "_id" }, "_id=?", new String[] { id }, null, null, null);
        int n = cursor.getCount();
        cursor.close();
        
        mDB = getWriteableDB();
        mDB.beginTransaction();
        try {
            if (n == 0) {
                values.put("_id", id);
                mDB.insert(tablename, null, values);
            } else {
                //values.remove("_id");
                mDB.update(tablename, values, "_id=?", new String[] { id });
            }
            mDB.setTransactionSuccessful();
        } finally {
            mDB.endTransaction();
        }
    }

	private void addOrUpdateExistingSyncableObject(SyncableObject syncableObject) {
		addOrUpdateTable(syncableObject.getTableName(), syncableObject.getContentValues(), syncableObject.getId());
	}

	private void addNewSyncableObject(SyncableObject syncableObject){
		SQLiteDatabase mDB = getWriteableDB();
		mDB.beginTransaction();
		try {
			mDB.insert(syncableObject.getTableName(), null, syncableObject.getContentValues());
			mDB.setTransactionSuccessful();
		} finally {
			mDB.endTransaction();
		}
	}

	public Cursor queryAllCategories(){
		return getReadableDB().rawQuery("Select * from Category", null);
	}

	public Cursor queryAllExpenses(Calendar cal){
		cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);

		// get start of the month
		cal.set(Calendar.DAY_OF_MONTH, 1);
		long startTime = cal.getTimeInMillis();
		
		cal.add(Calendar.MONTH, 1);
		long endTime = cal.getTimeInMillis();
		
		return getReadableDB().rawQuery("Select * from Expense where expenseDate > "+startTime+" and expenseDate < "+endTime+" order by creationDate desc", null);
		//return getReadableDB().rawQuery("Select * from Expense order by creationDate desc", null);
	}

	public Cursor queryAllSyncable(Class className) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		return getReadableDB().rawQuery("Select * from "+className.getField("tableName").get(null)+" where isSync=0", null);
	}
	
	public Cursor queryAllSyncing(Class className) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		return getReadableDB().rawQuery("Select * from "+className.getField("tableName").get(null)+" where isSync=5", null);
	}
}