package com.trickle.beta;

import java.util.Random;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockActivity;
import com.trickle.beta.rest.RESTException;
import com.trickle.beta.rest.RESTQueryDatabase;
import com.trickle.beta.rest.RESTRequestListeners;
import com.trickle.beta.rest.RESTServiceHelper;


public class SignupActivity extends SherlockActivity{
    /** Called when the activity is first created */
	
	ProgressDialog loggingProgressDialog;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.signup);
    }
	
	public void onLogin(View view){
		if(!validate())
    		return;
    	
		this.loggingProgressDialog = new ProgressDialog(this);
		this.loggingProgressDialog.setMessage(" Logging in ...");
		this.loggingProgressDialog.show();
		this.loggingProgressDialog.setCancelable(false);
		
    	String username = ((EditText)findViewById(R.id.username)).getText().toString();
    	String password = ((EditText)findViewById(R.id.password)).getText().toString();
    	String email = ((EditText)findViewById(R.id.email_address)).getText().toString();
    	RESTServiceHelper.getRESTServiceHelper().signUp(this, email, username, password, signupRESTListener);
    	
    	findViewById(R.id.username).setEnabled(false);
    	findViewById(R.id.password).setEnabled(false);
    	findViewById(R.id.confirm_password).setEnabled(false);
    	findViewById(R.id.email_address).setEnabled(false);
    	findViewById(R.id.login).setEnabled(false);
	}
	
	public boolean validate(){
		if(((EditText)findViewById(R.id.username)).getText().toString().trim().length() == 0){
			((EditText)findViewById(R.id.username)).setError(TrickleApp.getSpannableStringBuilder("Please enter username"));
			return false;
		}
		if(((EditText)findViewById(R.id.password)).getText().toString().length() == 0){
			((EditText)findViewById(R.id.password)).setError(TrickleApp.getSpannableStringBuilder("Please enter password."));
			return false;
		}
		
		if(((EditText)findViewById(R.id.confirm_password)).getText().toString().length() == 0){
			((EditText)findViewById(R.id.confirm_password)).setError(TrickleApp.getSpannableStringBuilder("Please confirm your password."));
			return false;
		}
		if(((EditText)findViewById(R.id.email_address)).getText().toString().length() == 0){
			((EditText)findViewById(R.id.email_address)).setError(TrickleApp.getSpannableStringBuilder("Please enter you email address"));
			return false;
		}
		
		if(!((EditText)findViewById(R.id.password)).getText().toString().equals(((EditText)findViewById(R.id.confirm_password)).getText().toString())){
			((EditText)findViewById(R.id.confirm_password)).setError(TrickleApp.getSpannableStringBuilder("Your passwords must match!"));
			return false;
		}
		return true;
	}
	
	RESTRequestListeners signupRESTListener = new RESTRequestListeners() {
		
		@Override
		public int requestCompleted(int requestId, int responseCode) {
			loggingProgressDialog.dismiss();
			if(responseCode == HttpStatus.SC_OK){
				startActivity(new Intent(getApplicationContext(), LoginActivity.class));
				finish();
			}else{
				showErrorMessage(responseCode, "Error");
				return -1;
			}
			return 0;
		}
	};
	
	public void showErrorMessage(int responseCode, String message){
		TrickleApp.showToast(this, responseCode+": " + message);
	}
	
	public void onExistingUser(View view){
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}
}