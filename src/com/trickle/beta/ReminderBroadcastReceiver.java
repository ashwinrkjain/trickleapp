package com.trickle.beta;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

/**
 * Created by ashwinjain on 29/05/13.
 */
public class ReminderBroadcastReceiver extends BroadcastReceiver {

    NotificationManager nm;

    @Override
    public void onReceive(Context context, Intent intent) {
        Vibrator vibrator=(Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        nm=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        CharSequence ch="Expense update reminder";
        CharSequence message="Update today's expenses";
        Notification notif = new Notification(R.drawable.app_icon,
                "Trickle reminder", System.currentTimeMillis());
//       PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//                  new Intent(), 0);
        Intent notifyintent=new Intent(context,ReminderBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notifyintent, android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
        notif.flags |=Notification.FLAG_AUTO_CANCEL;
        notif.setLatestEventInfo(context, ch, message, pendingIntent);
        nm.notify(0, notif);
    }


    /*public void setAlarm(Context context)
    {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.FALSE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 5 seconds
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 5 , pi);
    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void setOnetimeTimer(Context context){
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pi);
    }*/
}