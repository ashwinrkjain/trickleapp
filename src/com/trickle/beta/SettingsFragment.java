package com.trickle.beta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.trickle.beta.adapters.DropDownCategoryAdapter;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ashwinjain on 29/05/13.
 */
public class SettingsFragment extends Fragment {

    Intent intent;


    @Override
    public void onAttach(Activity activity) {
        intent = activity.getIntent();
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        getActivity().setTitle("Settings");

        View finalView = inflater.inflate(R.layout.settings, container, false);
        return finalView;
    }
}
