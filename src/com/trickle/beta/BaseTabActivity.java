package com.trickle.beta;

import java.util.Currency;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class BaseTabActivity extends BaseMenuFragmentActivity {

	Fragment activeFragment;
	public static String ACTIVE_TAB = "activeTab";

    public BaseTabActivity(){
        super(R.string.main_menu_title);
    }

    public BaseTabActivity(int titleRes) {
        super(titleRes);
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getSupportActionBar().hide();
		getSupportActionBar().setDisplayShowHomeEnabled(false);              
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		setContentView(R.layout.tab_navigation);
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// add tabs
		Tab tab1 = actionBar.newTab().setText("Expense").setTabListener(
				new TabListener<EnterExpenseFragment>(this, "tab1", EnterExpenseFragment.class));
		actionBar.addTab(tab1);

		Tab tab2 = actionBar.newTab()
				.setText("Reports")
				.setTabListener(new TabListener<CategoryReportFragment>(
						this, "tab2", CategoryReportFragment.class));
		
		actionBar.addTab(tab2);

		/*Tab tab3 = actionBar.newTab()
				.setText("Add Category")
				.setTabListener(new TabListener<EnterCategoryFragment>(
						this, "tab3", EnterCategoryFragment.class));
		actionBar.addTab(tab3);*/

		// check if there is a saved state to select active tab
		if( savedInstanceState != null ){
			getSupportActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(ACTIVE_TAB));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// save active tab
		outState.putInt(ACTIVE_TAB,
				getSupportActionBar().getSelectedNavigationIndex());
		super.onSaveInstanceState(outState);
	}

	protected class TabListener<T extends Fragment> implements ActionBar.TabListener {

		private Fragment mFragment;
		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;

		public TabListener(Activity activity, String tag, Class<T> clz) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			// Check if the fragment is already initialized
			if (mFragment == null) {
				// If not, instantiate and add it to the activity
				mFragment = (Fragment) Fragment.instantiate(
						mActivity, mClass.getName());
				//mFragment.setProviderId(mTag); // id for event provider
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				// If it exists, simply attach it in order to show it
				ft.attach(mFragment);
			}

			activeFragment = mFragment;
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				// Detach the fragment, because another one is being attached
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// User selected the already selected tab. Usually do nothing.
		}
	}

	/*public void createCategory(View view){
		((EnterCategoryFragment)activeFragment).createCategory();
	}*/

	public void onCreateExpense(View view){
		((EnterExpenseFragment)activeFragment).onCreateExpense();
	}
	
	public void onClearExpense(View view){
		((EnterExpenseFragment)activeFragment).onClearExpense();
	}
	
	public void onAddCategory(View view){
		((EnterExpenseFragment)activeFragment).onAddCategory(view);		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.ENTER_CATEGORY_FROM_EXPENSE) {
			((EnterExpenseFragment)activeFragment).handleAddCategoryResponse(resultCode, data);
		}
	}
	
	public void onClickExpenseDate(View view){
		((EnterExpenseFragment)activeFragment).onClickExpenseDate(view);		
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		return ((EnterExpenseFragment)activeFragment).onCreateDialog(id);
	}
	
}
