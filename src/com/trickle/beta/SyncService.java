package com.trickle.beta;

import java.util.Date;
import java.util.Random;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.graphics.Paint.Join;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Category;
import com.trickle.beta.model.Expense;
import com.trickle.beta.rest.RESTException;
import com.trickle.beta.rest.RESTQueryDatabase;
import com.trickle.beta.rest.RESTRequestListeners;
import com.trickle.beta.rest.RESTServiceHelper;

public class SyncService  extends Service{
	 
	private final String LOG_TAG = "SyncService";

	private final IBinder mBinder = new SyncServiceBinder();


	@Override
	public void onCreate(){
		super.onCreate();
		Log.i(LOG_TAG, "onCreate()");
	}

	public int onStartCommand(Intent intent, int flags, int startId){
		if(intent==null){
			Log.i(LOG_TAG, "null intent");
		}else{
			Log.i(LOG_TAG, "processing intent");
			//send sync request
			
			//Get data source
			BucketDataSource dataSource = BucketDataSource.getBucketDataSource((TrickleApp)getApplication()); 
			
			RESTServiceHelper restServiceHelper = RESTServiceHelper.getRESTServiceHelper();
			
			JSONObject json = SyncServiceHelper.syncAll(Category.class, dataSource, this, null);
			json = SyncServiceHelper.syncAll(Expense.class, dataSource, this, json);
			try {
				json.put("last_sync_time", ((TrickleApp)getApplication()).getLastSyncTime());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			Log.e(this.getClass().getSimpleName(), json.toString());
			restServiceHelper.syncAll(this, ((TrickleApp)getApplication()).getToken(), json.toString(), requestListener);
			
		}
		return START_REDELIVER_INTENT;
	}
	

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Log.i(LOG_TAG, "onDestroy()");
	}

	public class SyncServiceBinder extends Binder {
		SyncService getService() {
			return SyncService.this;
		}
	}

	RESTRequestListeners requestListener = new RESTRequestListeners() {
		
		@Override
		public int requestCompleted(int requestCode, int responseCode) {
			if(responseCode == HttpStatus.SC_OK){
				//Query rest DB to get response
				RESTQueryDatabase db = new RESTQueryDatabase(getApplicationContext());
				try {
					long randomToken = new Random().nextLong();
					db.open(randomToken);
					String serverResponse = db.getServerResponse(requestCode);
					db.close(randomToken);
					handleResponse(serverResponse);
				} catch (RESTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				return -1;
			}
			return 0;
		}
	};
	
	public void handleResponse(String serverResponse) throws JSONException{
		JSONObject jsonResponse = new JSONObject(serverResponse);
		SyncServiceHelper.handleResponse(jsonResponse, getApplication());
		
		((TrickleApp)getApplication()).setLastSyncTime(jsonResponse.getLong("last_sync_time"));
	}
	
}
