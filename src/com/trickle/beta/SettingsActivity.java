package com.trickle.beta;

import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;

/**
 * Created by ashwinjain on 29/05/13.
 */
public class SettingsActivity extends BaseMenuFragmentActivity {
    public SettingsActivity(){
        super(R.string.main_menu_title);
    }

    public SettingsActivity(int titleRes) {
        super(titleRes);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        setTitle("Settings");
        setContentView(R.layout.settings);
    }
}
