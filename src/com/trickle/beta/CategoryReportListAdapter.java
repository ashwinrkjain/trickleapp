package com.trickle.beta;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trickle.beta.model.Category;
import com.trickle.beta.ui.TextProgressBar;

public class CategoryReportListAdapter extends BaseAdapter{
	ArrayList<Category> categories;
	Activity context;

	public CategoryReportListAdapter(ArrayList<Category> categories, Activity context){
		this.categories = categories;
		this.context = context;
	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Object getItem(int index) {
		return categories.get(index);
	}

	@Override
	public long getItemId(int index) {
		return categories.get(index).getId().hashCode();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = context.getLayoutInflater();
		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(R.layout.category_listitem, null);
		}

		TextView categoryTitle = (TextView)view.findViewById(R.id.categoryTitle);
		categoryTitle.setText(categories.get(position).getCategoryName());
		
		TextProgressBar progressBar = (TextProgressBar)view.findViewById(R.id.progressBarCategoryBudget);
		
		if(categories.get(position).getCategoryBudget() > 0){
			progressBar.setMax((int)Math.round(categories.get(position).getCategoryBudget()));
			progressBar.setProgress((int)Math.round(categories.get(position).getCategoryExpenditure()));
			float progress = (float)progressBar.getProgress()/ (float) progressBar.getMax();
			setProgressBarColor(progressBar, Color.HSVToColor(new float[]{(1-(float)progress)*120f,1f,0.75f}));
			progressBar.setTextColor(Color.HSVToColor(new float[]{1f,0f,Math.round(progress)}));
			progressBar.setText(categories.get(position).getCategoryExpenditure()+"/" +categories.get(position).getCategoryBudget());
			//progressBar.setProgressDrawable(new ColorDrawable(;
			
			
		} else {
			progressBar.setText(categories.get(position).getCategoryExpenditure()+"");
			progressBar.setMax(100);
			progressBar.setProgress(0);
		}
		return view;
	}
	
	public void setProgressBarColor(ProgressBar progressBar, int newColor){ 
	    LayerDrawable ld = (LayerDrawable) progressBar.getProgressDrawable();
	    ClipDrawable d1 = (ClipDrawable) ld.findDrawableByLayerId(R.id.progress);
	    d1.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);

	}
}
