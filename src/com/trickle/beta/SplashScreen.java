package com.trickle.beta;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;


public class SplashScreen extends SherlockActivity {
    
    protected boolean _active = true;
    protected int _splashTime = 1500; // time to display the splash screen in ms
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        
        final Class launchClass;
    	if(getApp().haveToken()){
    		launchClass = BaseActivity.class;
    		startService(new Intent(getApplicationContext(), SyncService.class));
    	}else{
    		launchClass = SignupActivity.class;
    	}
        
        // thread for displaying the SplashScreen
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {
                    
                } finally {
                	startActivity(new Intent(SplashScreen.this, launchClass));
                    finish();
                    interrupt();
                }
            }

			
        };
        splashTread.start();
    }
    private TrickleApp getApp() {
		return (TrickleApp)getApplication();
	}
}
