package com.trickle.beta;

/**
 * Created by ashwinjain on 30/05/13.
 */
public interface SlideMenuItemSelectedListener {
    public void onSlideMenuItemSelected(SlideMenuItem selectedItem);
}
