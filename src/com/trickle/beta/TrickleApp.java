package com.trickle.beta;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.Toast;

import com.trickle.beta.database.BucketDataSource;

import java.util.Calendar;
import java.util.Date;

public class TrickleApp extends Application{
	
	BucketDataSource dataSource;
	
	String sharedPrefTag = "TrickleApp";
	String TOKEN_KEY= "token_key";
	String HAVE_TOKEN_KEY = "have_token_key";
	String LAST_SYNC_TIME_KEY = "last_sync_time_key";
	String token="";
	
	@Override
	public void onCreate(){
		super.onCreate();
        //Register for notification
        registerForReminder();

		dataSource = BucketDataSource.getBucketDataSource(this); 
	}
	
	public BucketDataSource getDataSource(){
		return this.dataSource;
	}
	
	public static SpannableStringBuilder getSpannableStringBuilder(String text){
		int ecolor = Color.WHITE; // whatever color you want
		String estring = text;
		ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
		SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
		ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
		return ssbuilder;
	}
	
	public static void showToast(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

    public void registerForReminder(){
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getApplicationContext(), 0, new Intent(getApplicationContext(), ReminderBroadcastReceiver.class), 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        Date dat = new Date();
        Calendar alarmTime = Calendar.getInstance();
        alarmTime.setTime(dat);
        alarmTime.set(Calendar.HOUR_OF_DAY, 21);
        alarmTime.set(Calendar.MINUTE, 0);
        alarmTime.set(Calendar.SECOND, 0);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), 24*60*60*1000, pendingIntent);
    }
	
	public void setToken(String token) {
		this.token = token;
		
        SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(TOKEN_KEY, token);
        prefsEditor.putBoolean(HAVE_TOKEN_KEY, true);
        SharedPreferencesCompat.apply(prefsEditor);
    }
    
    public String getToken() {
    	if(token.equals("")){
    		SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
    		token =prefs.getString(TOKEN_KEY, ""); 
    	}
        return token;
    }
    
    public boolean haveToken(){
    	SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        return prefs.getBoolean(HAVE_TOKEN_KEY, false);
    }
    
    public void setLastSyncTime(long lastSyncTime) {
		SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putLong(LAST_SYNC_TIME_KEY, lastSyncTime);
        SharedPreferencesCompat.apply(prefsEditor);
    }

	public long getLastSyncTime() {
		SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        return prefs.getLong(LAST_SYNC_TIME_KEY, 0);
	}

    public void addUsedLocations(String location){
        SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        String str=prefs.getString(Constants.SHARED_PREF_LOCATION_LIST, "");
        if(str.length()==0) str=location;
        else str=str+","+location;

        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(Constants.SHARED_PREF_LOCATION_LIST, str);
        SharedPreferencesCompat.apply(prefsEditor);
    }

    public String[] getUsedLocationList(){
        SharedPreferences prefs = getSharedPreferences(sharedPrefTag, MODE_PRIVATE);
        return prefs.getString(Constants.SHARED_PREF_LOCATION_LIST, "").split(",");
    }
}
