package com.trickle.beta;

/**
 * Created by ashwinjain on 30/05/13.
 */
public class SlideMenuItem {
    public String tag;
    public int iconRes;
    public boolean isTitle;
    public SlideMenuItem(String tag, int iconRes, boolean isTitle) {
        this.tag = tag;
        this.iconRes = iconRes;
        this.isTitle = isTitle;
    }
}
