package com.trickle.beta.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.database.BucketDatabase;
import com.trickle.beta.model.SyncableObject.Condition;

import android.content.Context;
import android.database.Cursor;

public class SyncableObjectFactory {

	public static SyncableObject getObjectFromCursor(Cursor cursor, Class className){
		SyncableObject syncableObject = null;
		try {
			Constructor constructor = className.getConstructor(Cursor.class);
			syncableObject = (SyncableObject) constructor.newInstance(cursor);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return syncableObject;
		
	}
	
	public static final ArrayList<SyncableObject> getSyncableObjects(BucketDataSource dataSource, Context context, Class className) 
			throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		ArrayList<SyncableObject> syncableObjects = new ArrayList<SyncableObject>();
		BucketDatabase db = dataSource.getDatabase(context);
		Cursor cursor = null;
		
		cursor = db.queryAllSyncable(className);
    	
		if(cursor.moveToFirst()){
	    	for (int i = 0; i < cursor.getCount(); i++) {
	    		SyncableObject syncableObject = getObjectFromCursor(cursor, className);
	    		syncableObjects.add(syncableObject);
	            cursor.moveToNext();
	        }
		}
    	return syncableObjects;
	}
	
	public static final ArrayList<SyncableObject> getSyncingObjects(BucketDataSource dataSource, Context context, Class className) 
			throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		ArrayList<SyncableObject> syncableObjects = new ArrayList<SyncableObject>();
		BucketDatabase db = dataSource.getDatabase(context);
		Cursor cursor = null;
		
		cursor = db.queryAllSyncing(className);
    	
		if(cursor.moveToFirst()){
	    	for (int i = 0; i < cursor.getCount(); i++) {
	    		SyncableObject syncableObject = getObjectFromCursor(cursor, className);
	    		syncableObjects.add(syncableObject);
	            cursor.moveToNext();
	        }
		}
    	return syncableObjects;
	}
}
