package com.trickle.beta.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.JetPlayer;

import com.trickle.beta.TrickleApp;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.database.BucketDatabase;

public abstract class SyncableObject implements Serializable{
	private String id;
	private int isSynced;
	private Date updateDate;
	private Date creationDate;
	
	private static String tableName;
	
	public enum Condition { NEW, UPDATED, UNCHANGED, ALL};
	
	public SyncableObject(){
		super();
	}
	
	/**
	 * DOES NOTHING!! To be implemented in all child classes
	 * @param cursor
	 */
	@Deprecated
	public SyncableObject(Cursor cursor) {
		
	}
	
	public SyncableObject(String uniqueID, int isSynced,
			Date creationDate, Date updateDate) {
		super();
		this.setId(uniqueID);
		this.setSynced(isSynced);
		this.setCreationDate(creationDate);
		this.setUpdateDate(updateDate);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int isSynced() {
		return isSynced;
	}

	public void setSynced(int isSynced) {
		this.isSynced = isSynced;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public abstract String getTableName();

	@Deprecated
	public JSONObject convertToJSON() throws IllegalArgumentException, JSONException, IllegalAccessException{
		JSONObject jsonObject = new JSONObject();
		Field[] allFields = this.getClass().getFields();
		Field field = null;
		for(int i=0; i < allFields.length ; i++){
			field = allFields[i];
			if(!Modifier.isStatic(field.getModifiers())){
				if(field.get(this) instanceof Date){
					jsonObject.put(field.getName(), ((Date)field.get(this)).getTime());
				} else {					
					jsonObject.put(field.getName(), field.get(this).toString());
					}	
			}
		}
		return jsonObject;
	}
	
	public JSONObject getStatusFields() throws NoSuchFieldException, JSONException{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(this.getClass().getSimpleName().toLowerCase()+"_unique_id", id);
		jsonObject.put(this.getClass().getSimpleName().toLowerCase()+"_update_date", updateDate.getTime());
		return jsonObject;
	}
	
	public void save(Application application){
		BucketDataSource bucketDataSource = BucketDataSource.getBucketDataSource((TrickleApp) application);
		bucketDataSource.addSyncableObject(this, application.getApplicationContext());
	}

    public void delete(Application application){
        BucketDataSource bucketDataSource = BucketDataSource.getBucketDataSource((TrickleApp) application);
        bucketDataSource.removeSyncableObject(this, application.getApplicationContext());
    }
	
	public abstract ContentValues getContentValues();

	
}
