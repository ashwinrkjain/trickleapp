package com.trickle.beta.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;

public class Category extends SyncableObject{
	
	private String categoryName;
	private double categoryBudget;
	private String categoryDescription;
	private Collection<Expense> expenses;
	private Map<String,Expense> expensesMap;
	
	public static String tableName = "Category";
	
	public Category(String id, String categoryName, double categoryBudget, String categoryDescription, int isSynced, 
			Date creationDate, Date updateDate ) {
		super(id, isSynced, creationDate, updateDate);
		this.categoryName = categoryName;
		this.categoryBudget = categoryBudget;
		this.categoryDescription = categoryDescription;

		expensesMap = new HashMap<String, Expense>();
		expenses = expensesMap.values();
	}
	
	public Category(Cursor cursor){
		this(cursor.getString(cursor.getColumnIndex("_id")), 
				cursor.getString(cursor.getColumnIndex("name")), 
				cursor.getDouble(cursor.getColumnIndex("budget")), 
				cursor.getString(cursor.getColumnIndex("description")), 
				cursor.getInt(cursor.getColumnIndex("isSync")), 
				new Date(cursor.getLong(cursor.getColumnIndex("creationDate"))),
				new Date(cursor.getLong(cursor.getColumnIndex("updateDate"))));
	}
	
	public Category(JSONObject jsonCategory) throws JSONException {
		this(jsonCategory.getString("category_id"),
				jsonCategory.getString("category_name"),
				jsonCategory.getDouble("category_budget"),
				jsonCategory.getString("category_description"),
				10,
				new Date(jsonCategory.getLong("category_creation_date")),
				new Date(jsonCategory.getLong("category_update_date")));
	}

	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public double getCategoryBudget() {
		return categoryBudget;
	}
	public void setCategoryBudget(double categoryBudget) {
		this.categoryBudget = categoryBudget;
	}
	public String getCategoryDescription() {
		return categoryDescription;
	}
	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}
	public Collection<Expense> getExpenses() {
		return expenses;
	}
	public void setExpensesInThisCategory(Collection<Expense> expensesInThisCategory) {
		this.expenses = expensesInThisCategory;
	}
	public double getCategoryExpenditure() {
		double categoryExpenditure = 0;
		
		if(expenses != null){
			Iterator<Expense> iteratorOverExpenses = expenses.iterator();
			while(iteratorOverExpenses.hasNext()){
				categoryExpenditure = categoryExpenditure + iteratorOverExpenses.next().getExpenseAmount();
			}
		}
		
		return categoryExpenditure;
	}
	
	public void addExpense(Expense expense){
		expensesMap.put(expense.getId(), expense);
	}

    public void removeExpense(Expense expense){
        expensesMap.remove(expense.getId());
    }
	
	@Override
	public String getTableName() {
		return tableName;
	}
	


	@Override
	public ContentValues getContentValues() {
		ContentValues contentValues = new ContentValues();
		contentValues.put("_id", this.getId());
		contentValues.put("name", this.getCategoryName());
		contentValues.put("budget", this.getCategoryBudget());
		contentValues.put("description", this.getCategoryDescription());
		contentValues.put("isSync", this.isSynced());
		contentValues.put("creationDate", this.getCreationDate().getTime());
		contentValues.put("updateDate", this.getUpdateDate().getTime());
		
		return contentValues;
		
	}
	
	public JSONObject convertToJSON() throws JSONException{
		
		JSONObject jsonObject = new JSONObject();
		
		jsonObject.put("category_name", categoryName);
		jsonObject.put("category_budget", categoryBudget);
		jsonObject.put("category_description", categoryDescription);
		
		jsonObject.put("category_id", this.getId());
		jsonObject.put("category_creation_date", this.getCreationDate().getTime());
		jsonObject.put("category_update_date",this.getUpdateDate().getTime());
		
		return jsonObject;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Category){
			return (((Category)o).getId().equals(this.getId()));
		} else if (o instanceof String) {
			return(o.equals(this.getId()));
		}else {
			return false;
		}
	}
}
