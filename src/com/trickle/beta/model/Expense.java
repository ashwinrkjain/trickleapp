package com.trickle.beta.model;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;

public class Expense extends SyncableObject implements Serializable {
	
	public double expenseAmount;
	public String expenseDescription;
	public String expenseLocation;
	public String categoryId;
	public Date expenseDate;
	public double expenseLocationLat;
	public double expenseLocationLong;
	public static String tableName = "Expense";
	
	public Expense(String id, int isSynced,
			double expenseAmount, String expenseDescription, String expenseLocation, double expenseLocationLat, double expenseLocationLong,String categoryId,
			Date expenseDate, Date creationDate, Date updateDate) {
		super(id, isSynced, creationDate, updateDate);
		this.expenseAmount = expenseAmount;
		this.expenseDescription = expenseDescription;
		this.expenseLocation = expenseLocation;
		this.categoryId = categoryId;
		this.expenseDate = expenseDate;
		this.expenseLocationLat = expenseLocationLat;
		this.expenseLocationLong = expenseLocationLong;
	}
	
	public Expense(Cursor cursor) {
		this(cursor.getString(cursor.getColumnIndex("_id")),
				cursor.getInt(cursor.getColumnIndex("isSync")),
				cursor.getDouble(cursor.getColumnIndex("amount")),
				cursor.getString(cursor.getColumnIndex("description")),
				cursor.getString(cursor.getColumnIndex("location")),
				cursor.getDouble(cursor.getColumnIndex("locationLat")),
				cursor.getDouble(cursor.getColumnIndex("locationLong")),
				cursor.getString(cursor.getColumnIndex("categoryId")),
				new Date(cursor.getLong(cursor.getColumnIndex("expenseDate"))),
				new Date(cursor.getLong(cursor.getColumnIndex("creationDate"))),
				new Date(cursor.getLong(cursor.getColumnIndex("updateDate"))));
	}
	
	public Expense(JSONObject jsonExpense) throws JSONException {
		this(
				jsonExpense.getString("expense_id"),
				10,
				jsonExpense.getDouble("expense_amount"),
				jsonExpense.getString("expense_description"),
				jsonExpense.getString("expense_location"),
				jsonExpense.getDouble("expense_location_latitude"),
				jsonExpense.getDouble("expense_location_longitude"),
				jsonExpense.getString("category_id"),
				new Date(jsonExpense.getLong("expense_date")),
				new Date(jsonExpense.getLong("expense_creation_date")),
				new Date(jsonExpense.getLong("expense_update_date")));
	}

	public double getExpenseLocationLat() {
		return expenseLocationLat;
	}
	public void setExpenseLocationLat(double locationLat) {
		this.expenseLocationLat = locationLat;
	}
	public double getExpenseLocationLong() {
		return expenseLocationLong;
	}
	public void setExpenseLocationLong(double locationLong) {
		this.expenseLocationLong = locationLong;
	}
	
	public double getExpenseAmount() {
		return expenseAmount;
	}
	public void setExpenseAmount(double amount) {
		this.expenseAmount = amount;
	}
	public String getExpenseDescription() {
		return expenseDescription;
	}
	public void setExpenseDescription(String description) {
		this.expenseDescription = description;
	}
	public String getExpenseLocation() {
		return expenseLocation;
	}
	public void setExpenseLocation(String location) {
		this.expenseLocation = location;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public Date getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}
	
	public String getTableName() {
		return tableName;
	}

	public static void setTableName(String tableName) {
		Expense.tableName = tableName;
	}

	@Override
	public JSONObject convertToJSON() throws JSONException{
			
			JSONObject jsonObject = new JSONObject();
			
			jsonObject.put("expense_amount", expenseAmount);
			jsonObject.put("expense_description", expenseDescription);
			jsonObject.put("expense_location", expenseLocation);
			jsonObject.put("expense_category_id", categoryId);
			jsonObject.put("expense_date", expenseDate.getTime());
			jsonObject.put("expense_location_latitude", expenseLocationLat);
			jsonObject.put("expense_location_longitude", expenseLocationLong);
			
			jsonObject.put("expense_id", this.getId());
			jsonObject.put("expense_creation_date", this.getCreationDate().getTime());
			jsonObject.put("expense_update_date",this.getUpdateDate().getTime());
			
			return jsonObject;
	}
	
	
	
	public ContentValues getContentValues(){
		ContentValues vals = new ContentValues();
		vals.put("_id", this.getId());
		vals.put("isSync", this.isSynced());
		vals.put("amount", this.getExpenseAmount());
		vals.put("description", this.getExpenseDescription());
		vals.put("location", this.getExpenseLocation());
		vals.put("locationLat", this.getExpenseLocationLat());
		vals.put("locationLong", this.getExpenseLocationLong());
		vals.put("categoryId", this.getCategoryId());
		vals.put("expenseDate", this.getExpenseDate().getTime());
		vals.put("creationDate", this.getCreationDate().getTime());
		vals.put("updateDate", this.getUpdateDate().getTime());
		return vals;
	}
	
}