package com.trickle.beta;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.fima.cardsui.objects.Card;
import com.fima.cardsui.views.CardUI;
import com.trickle.beta.database.BucketDataSource;
import com.trickle.beta.model.Expense;
import com.trickle.beta.ui.TouchListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by ashwinjain on 30/05/13.
 */
public class ExpenseCardsActivity extends SherlockActivity{

    public static final String EXPENSE_POSITION = "Expense_Position";
    Expense[] expenses = null;
    ArrayList<Expense> arrayListOfExpenses = null;

    CardUI mCardView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.expense_card_list);

        // init CardView
        mCardView = (CardUI) findViewById(R.id.cardsview);
        mCardView.setSwipeable(true);

        Intent intent = getIntent();
        String expensePosition = intent.getStringExtra(EXPENSE_POSITION);

        BucketDataSource bucketDataSource = BucketDataSource.getBucketDataSource((TrickleApp)getApplication());
        if(expensePosition == null || "-1".equals(expensePosition)){
            arrayListOfExpenses = new ArrayList<Expense>(bucketDataSource.getExpenses());
        } else {
            arrayListOfExpenses = new ArrayList<Expense>(bucketDataSource.getExpensesOfCategory(expensePosition));
        }
        Collections.sort(arrayListOfExpenses, dateComparator);
        expenses = arrayListOfExpenses.toArray(new Expense[arrayListOfExpenses.size()]);
        for(int i=0;i<expenses.length;i++){
            ExpenseCard eCard= new ExpenseCard(expenses[i]);
            mCardView.addCard(eCard);

            final int finalI = i;
            eCard.setOnCardSwipedListener(new Card.OnCardSwiped() {

				@Override
				public void onCardSwiped(Card card, View layout) {
                    expenses[finalI].delete(getApplication());
                    Toast.makeText(ExpenseCardsActivity.this, expenses[finalI].getExpenseLocation(), Toast.LENGTH_LONG).show();;
				}
            });
        }

        // draw cards
        mCardView.refresh();
    }

    Comparator<Expense> dateComparator = new Comparator<Expense>() {

        @Override
        public int compare(Expense lhs, Expense rhs) {
            if(lhs.getCreationDate().before(rhs.getCreationDate())){
                return -1;
            } else if (lhs.getCreationDate().after(rhs.getCreationDate())){
                return 1;
            } else{
                return 0;
            }
        }
    };
}
