package com.trickle.beta;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fima.cardsui.objects.Card;
import com.fima.cardsui.views.CardUI;
import com.trickle.beta.model.Expense;

/**
 * Created by ashwinjain on 30/05/13.
 */
public class ExpenseCard extends Card {

    Expense expense;
    CardUI cardsUI;

    public ExpenseCard(Expense expense){
        super(expense.getExpenseLocation());
        this.expense = expense;
    }

    @Override
    public View getCardContent(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_expense, null);
        ((TextView) view.findViewById(R.id.card_expense_title)).setText(expense.getExpenseLocation());
        ((TextView) view.findViewById(R.id.card_expense_time)).setText(expense.getExpenseDate().toGMTString());
        ((TextView) view.findViewById(R.id.card_expense_amount)).setText(expense.getExpenseAmount()+"");
        ((TextView) view.findViewById(R.id.card_expense_description)).setText(expense.getExpenseDescription());

        ((Button) view.findViewById(R.id.card_expense_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpenseCard.this.OnSwipeCard();
            }
        });

        return view;
    }


}
