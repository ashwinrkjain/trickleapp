package com.trickle.beta;

/**
 * Created by ashwinjain on 28/05/13.
 */
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.trickle.beta.adapters.SlideMenuListAdapter;

import static com.trickle.beta.Constants.MENU_ADD_EXPENSE;
import static com.trickle.beta.Constants.MENU_CATEGORY_ALL;
import static com.trickle.beta.Constants.MENU_CATEGORY_TITLE;
import static com.trickle.beta.Constants.MENU_EXPENSE_TITLE;
import static com.trickle.beta.Constants.MENU_SETTINGS_TITLE;

public class MainMenuListFragment extends ListFragment {

    SlideMenuItemSelectedListener slideMenuItemSelectedListener=null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sm_list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SlideMenuListAdapter adapter = new SlideMenuListAdapter(getActivity());
        /*for (int i = 0; i < 20; i++) {
            adapter.add(new com.trickle.beta.SlideMenuItem("Sample List", android.R.drawable.ic_menu_search));
        }*/
        adapter.add(new SlideMenuItem(MENU_EXPENSE_TITLE, -1, true));
        adapter.add(new SlideMenuItem(MENU_ADD_EXPENSE, R.drawable.ic_new, false));
        adapter.add(new SlideMenuItem(MENU_CATEGORY_TITLE, -1, true));
        adapter.add(new SlideMenuItem(MENU_CATEGORY_ALL, R.drawable.ic_storage, false));
        adapter.add(new SlideMenuItem(MENU_SETTINGS_TITLE, R.drawable.ic_settings, false));
        adapter.add(new SlideMenuItem("Logout", R.drawable.ic_off, false));
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        SlideMenuItem item = (SlideMenuItem) getListView().getItemAtPosition(position);
        ((BaseActivity)getActivity()).getSlidingMenu().setSelectedView(v);
        slideMenuItemSelectedListener.onSlideMenuItemSelected(item);
    }

    public void setSlideMenuItemSelectedListener(SlideMenuItemSelectedListener slideMenuItemSelectedListener){
        this.slideMenuItemSelectedListener = slideMenuItemSelectedListener;
    }
}
